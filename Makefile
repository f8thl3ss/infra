.PHONY: init docker-machine clean 

init:
	terraform -chdir=./terraform init

docker-machine:
	terraform apply -var "do_token=${DIGITALOCEAN_ACCESS_TOKEN}" -var "pvt_key=${HOME}/.ssh/id_rsa" -chdir=./terraform

clean:
	terraform destroy -var "do_token=${DIGITALOCEAN_ACCESS_TOKEN}" -var "pvt_key=${HOME}/.ssh/id_rsa" -chdir=./terraform

